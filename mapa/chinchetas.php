<?php
session_start();

if (!isset($_SESSION["coordenadas"]) ){
    $coordenadas = array();
    $_SESSION["coordenadas"]= $coordenadas;
}

if (isset($_REQUEST["localiza"]) && !empty($_REQUEST["localiza"])){
    $direccion = $_REQUEST["localiza"];
    $direc = urlencode($direccion);

    $url = "http://maps.google.com/maps/api/geocode/json?address=$direc";

    $json = file_get_contents($url);


    if ($json) {
        $json = json_decode($json); 
         if (json_last_error() == JSON_ERROR_NONE) {
            $lat = $json->results[0]->geometry->location->lat;
            $lng = $json->results[0]->geometry->location->lng;
        
             if ($lat != null && $lng != null){
                $coordenadas = $_SESSION["coordenadas"];
                $coordenadas[] =   ["lat" => $lat, "lng" => $lng];
        
       
                $_SESSION["coordenadas"]= $coordenadas;
        
        
                echo json_encode($coordenadas);
        
           }
        }
            else 
                trigger_error("formato no valido", E_USER_WARNING);
    }
    else 
        trigger_error("acceso no valido ", E_USER_WARNING);;
}   
