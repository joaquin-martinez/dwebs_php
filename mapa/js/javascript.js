  var locations; 

$(document).ready(function(){
    $("#insertar").click(function(){

    	localizacion = $("#localizacion").val();
    	$.getJSON( "chinchetas.php" , { "localiza" : localizacion } , function(resp){
    		console.log(resp);
    		locations = resp;
    	initMap();

    		
    	} );
    	
    });
});


function initMap() {

    var map = new google.maps.Map(document.getElementById('mapa'), {
      zoom: 5,
      center: {lat: 40.4530541, lng: -3.6883445}  
    });

    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    var markers = locations.map(function(location, i) {
      return new google.maps.Marker({
        position: location,
        label: labels[i % labels.length]
      });
    });

 
    var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
  }
