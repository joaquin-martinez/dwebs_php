DROP DATABASE IF EXISTS tienda2;
CREATE DATABASE tienda2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE tienda2;

-- TABLAS
DROP TABLE IF EXISTS categoria;
CREATE TABLE categoria (
	categoria varchar(50) NOT NULL,
	PRIMARY KEY (categoria)
);

DROP TABLE IF EXISTS articulo;
CREATE TABLE articulo (
	id int(10) NOT NULL,
	codigo varchar(30) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(50) NOT NULL,
	existencia double NOT NULL,
	precio double NOT NULL,
	categoria varchar(50),
	PRIMARY KEY (id),
	FOREIGN KEY (categoria) REFERENCES categoria(categoria)
);


-- DATOS
INSERT INTO categoria VALUES ('SOBRE'),('ARCHIVADOR'),('CARPETA'),('CAJA'),('CUADERNO'),('LAPIZ');

INSERT INTO articulo (id, codigo, nombre, descripcion, existencia, precio, categoria) VALUES
(3, 'EC003', 'CAJA LAZOO', 'CAJA 100X200', 10, 1.25, 'CAJA'),
(4, 'EC001', 'ESFERO ROJO', 'ESFERO BORRABLE', 40, 0.65, 'CAJA'),
(5, 'EC002', 'ESFERO NEGRO', 'ESFERO BORRABLE', 30, 0.65, 'CAJA'),
(6, 'FA001', 'FOLDER ARCHIVADOR', 'FOLDER CARTON', 10, 2.79, 'ARCHIVADOR'),
(7, 'FA002', 'FOLDER ANILLAS', 'SEPARADORA', 20, 3.79, 'ARCHIVADOR'),
(8, 'SM001', 'SOBRE MANILA ', 'SOBRE MANILA OFICIO', 15, 0.1, 'SOBRE');