<?php
include "math_functions.php";

echo digitos(123456) . "<br>";
var_dump(esCapicua(123321));
if(esCapicua(123521)) echo "capicua";
else echo "no capicua";
echo "<br>";
echo "<br>";
echo "solucion<br>";
if(esCapicua("ana")) echo "capicua";
else echo "no capicua";
echo "<br>";

echo "<br>";

if(esPrimo(18)){
  echo "Es primo.";
}else{
  echo "No primo.";
}
echo "<br>";
echo "<br>";
echo "la posicion es" . digitoN(12345 , 4);
echo "<br>";
echo "<br>";
echo "siguiente primo " . siguientePrimo(7);
echo "<br>";
echo "<br>";
echo "potencia 2 4 = " . potencia(2,4);
echo "<br>";
echo "<br>";
echo "la posicion primera del 5 es 3 = " . posicionDeDigito(1235458,5);
echo "<br>";
echo "<br>";
echo "pego 1234 a 5678 por detras: " . pegaPorDetras(1234, 5678);
echo "<br>";
echo "<br>";
echo "pego 5678 a 1234 por delante: " . pegaPorDelante(5678 , 1234);
echo "<br>";
echo "<br>";
echo "trozo numero 5678 a 1 . 3 : " . trozoDeNumero(5678 , 1 , 7);
