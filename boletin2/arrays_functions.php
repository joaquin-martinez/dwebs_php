<?php
/*
Dado un array y un color, devuelve su contenido en una estructura <table>
sombreando, en el color dado, s�lo filas pares (usa estilos CSS).
*/
function sombreaTabla($origenArray , $color){
  if(is_array($origenArray)){
    $salida = "<table> \n\r";
    foreach ($origenArray as $indice=>$valor){
      if($indice % 2 == 0){
          $salida .= "<tr class=\"par\" style=\"background:$color\"> \n\r";
      }else{
      $salida .= "<tr class=\"impar\" > \n\r";
      }
      if(is_array($valor)){
          foreach ($valor as $key => $value) {
            $value = preg_replace("/img\/.+/" , "<img src='$value' width='150vw' />" , $value);
            $salida .= "<td> $value </td> \n\r";
        }
        $salida .= "</tr> \n\r";
//          }

} else {
      $salida .= "<td> $valor </td> \r\n </tr>" ;
    }
    }
    $salida .= "</table>";
  }
  return $salida;
}





function sombreaTablaBi($origenArray , $color){

    if(is_array($origenArray)){
        $salida = "<table> \n\r";
        foreach ($origenArray as $indice=>$fila){
            if($indice % 2 == 0){
                $salida .= "<tr class=\"par\" style=\"background:$color\"> \n\r";
            }else{
            $salida .= "<tr class=\"impar\" > \n\r";
            }
            foreach($fila as $valor){
                $salida .= "<td> $valor </td> \n\r";
            }
            $salida .= "</tr> \n\r";
        }
        $salida .= "</table> \n\r";

    }
    return $salida ;

}

/*
Dado un array y una palabra devuelve el contenido del array en una estructura
<table> destacando la fila con la palabra encontrada o semejante a ella (usa estilos CSS).
*/
function destacaPalabra($unarray , $unapalabra){

    if(is_array($unarray)){
        $salida = "<table> \n\r";
        foreach($unarray as $indice=>$valor){

            if($unapalabra == $valor){
                $destacado="class=\"destacado\"";
            }else{
                $destacado="";
                }
            $salida .= "<tr $destacado > <td> $valor </td> </tr>";

        }

        $salida .= "</table>";
    }
    return $salida;
}

/*
 * Devuelve el contenido de un array bidimensional en un formato tabla.
 */
function generaTabla(){

    if(is_array($origenArray)){
        $salida = "<table> \n\r";
        foreach ($origenArray as $indice=>$fila){
            if($indice % 2 == 0){
                $salida .= "<tr class=\"par\" style=\"background:$color\"> \n\r";
            }else{
                $salida .= "<tr class=\"impar\" > \n\r";
            }
            foreach($fila as $valor){
                $salida .= "<td> $valor </td> \n\r";
            }
            $salida .= "</tr> \n\r";
        }
        $salida .= "</table> \n\r";

    }
    return $salida ;


}


/*
 * Dado un array bidimensional y un valor num�rico devuelve el contenido del array
en una estructura <table> sombreando aquellas filas que superen el valor dado (usa estilos CSS).
 */

function destacaValorMaximo($origenArray , $vmax){

    if(is_array($origenArray)){
        $salida = "<table> \n\r";
        foreach ($origenArray as $indice=>$fila){
           $linea ="";
           $destaca = false;
           $destacado = "";
            foreach($fila as $valor){
                if($valor > $vmax && $destaca==false ){
                    $destacado = "class=\"destacado\"";
                    $destaca=true;
//                }else{
//                    $destacado = "";
                }

                $linea .= "<td> $valor </td> \n\r";
            }
            $salida .= "<tr $destacado > $linea </tr> \n\r";
        }
        $salida .= "</table> \n\r";

    }
    return $salida ;



}
