﻿<?php

spl_autoload_register(function ($nombre_clase) {
  include '../modelo/' . $nombre_clase . '.php';
});

// require_once("funcionesAcciones.php");


if(isset($_REQUEST["accion"])){

  switch ($_REQUEST["accion"]) {
    case 'validar':
    accionValidar();
    break;
    case 'productos':
    accionProductos();
    break;
    case 'carrito':
    accionCarrito();
    break;
    case 'aCesta' :
    accionACesta();
    break;
    case 'salir' :
    accionSalir();
    break;
    default:
    require_once("control2.php");
    //      accionSalir();
    break;
  }
}else{
//  echo "Me viene por aqui.";
  require("vista/login.php");
}

function accionValidar(){

  $peticion = new ValidaCaptcha( 'https://www.google.com/recaptcha/api/siteverify' , $_POST["g-recaptcha-response"]);
  $noRobot = $peticion->comunica();

  if (is_readable("../config.ini")){
    $param = parse_ini_file("../config.ini" , true);
    echo " bd parametros " . $param['bd']['usuario'];
  }else {
    echo "falta fichero de configuracion";
  }




  $bd = new DAOImpl($param['bd']['tipo_de_base'].':host='.$param['bd']['host'].';dbname='.$param['bd']['nombre_de_base'], $param['bd']['usuario'], $param['bd']['contrasena']);


  $logado = $bd->validaUsuario($_REQUEST['username'] , $_REQUEST['password']) ;

  if($logado){
    session_start();
    $_SESSION['dao']= serialize($bd);
    $dao = unserialize($_SESSION['dao']);

    $_SESSION['usuario']=$_REQUEST['username'];
    $listaProductos = $dao->listarArticulos();

    require('../vista/catalogo.php');
  }else{
    $res= [ 'validacion' => "Usuario o clave incorectas." ];
    echo json_encode($res);
  }
}

function accionProductos(){

  session_start();
  $dao = unserialize($_SESSION['dao']);

  $listaProductos = $dao->listarArticulos();

  require('../vista/catalogo.php');

}


function accionCarrito(){
  session_start();
  $dao = unserialize($_SESSION['dao']);

  try{
    $listaCarrito = $dao->obtenCarrito($_SESSION['usuario']);
  } catch( Exception $e) {
    $listaCarrito = null;
  }

  require('../vista/carro.php');

}

function accionACesta(){

  session_start();
  $dao = unserialize($_SESSION['dao']);
  $articuloACesta['articulo'] = $_REQUEST['articulo'];
  $articuloACesta['unidades'] = $_REQUEST['unidades'];
  $articuloACesta['usuario'] = $_SESSION['usuario'];

  $unid = $dao->comprobarEnCarrito($articuloACesta['usuario'] , $articuloACesta['articulo']);

  if($unid[0] == 0){
    $dao->anadirCarrito($articuloACesta['usuario'] ,
    $articuloACesta['articulo'] , $articuloACesta['unidades'] );
  }else{
    $dao->cambiaUnidadesCarrito($articuloACesta['usuario'] ,
    $articuloACesta['articulo'] , ($articuloACesta['unidades'] + $unid[0]) );

  }

  echo "Se han añadido " . $articuloACesta['unidades'] . "unidades del articulo " . $articuloACesta['articulo'];
}

function accionSalir(){

  session_start();
  session_destroy();
  require("../vista/login.php");

}


function myFunctionErrorHandler($errno, $errstr, $errfile, $errline)
{
  //  echo "warning captcha";
}
