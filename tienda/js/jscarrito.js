$(document).ready(function(){

  $("#irCatalogo").click(function(){

    datos = {
      accion : 'productos'
    }

    $.post('controlador/control.php', datos , function(data) {

      $("#vista").html(data);
    } );

  });

  $("#salirCat").click(function(){

    datos = {
      accion : "salir"
    }

    $.post('controlador/control.php', datos , function(data) {

      $("#msg").html(data);



    } );

  });

  $(".bt-compra").click(function(event){
    codigoProd = this.getAttribute("data-reg");
    unidades = $("#uni-" + codigoProd).val();
    $("#div-carro-" + codigoProd).hide();

    datos = {
      accion : "comprar",
      articulo : codigoProd ,
      unidades : unidades
    }

    $.post('controlador/control.php', datos , function(data) {
      $("#msg").html(data);
    } );

  });

  $(".bt-borra").click(function(event){
    codigoProd = this.getAttribute("data-reg");
    $("#div-carro-" + codigoProd).hide();

    datos = {
      accion : "borrar",
      articulo : codigoProd
    }

    $.post('controlador/control.php', datos , function(data) {

      $("#msg").html(data);



    } );

  });



});
