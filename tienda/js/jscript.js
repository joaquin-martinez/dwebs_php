$(document).ready(function(){

  $('#fLogin').submit(function(e){
    e.preventDefault();

    fLogin.checkValidity();

    recapt = $("#g-recaptcha-response").val();
    if(recapt == "" ){
      $("#salida").html("Es necesario validar recaptcha.");
    }else {

      datos = $('#fLogin').serialize();

      alert(datos);

      $.post('controlador/control.php', datos , function(data) {

        if (data=='{"validacion":"Usuario o clave incorectas."}'){
          dat=JSON.parse(data);
          $("#salida").html( dat.validacion + " Intentelo de nuevo.");
          fLogin.reset();
          grecaptcha.reset();
        } else {
          $("#vista").html(data);
        }
      });

    }

  });

});
