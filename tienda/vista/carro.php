<script src="js/jscarrito.js" ></script>

<h1> CARRITO </h1>

<?php
if(isset($listaCarrito) ){

  foreach( $listaCarrito as $producto ){
    ?>


    <div class="prod" id="div-carro-<?= $producto['codigo'] ?>">
      <ul>
        <li>Codigo: <?= $producto['codigo'] ?> </li>
        <li>Nombre: <?= $producto['nombre'] ?> </li>
        <li>Descripcion: <?= $producto['descripcion'] ?> </li>
        <li>Precio unitario: <?= $producto['precio'] ?> </li>
        <li id="exis-<?= $producto['codigo'] ?>" data-ini= "<?= $producto['existencia'] ?>" >Disponibles: <?= $producto['existencia'] ?> </li>
        <li  ><input type="number" class="bt-uni" id="uni-<?= $producto['codigo'] ?>" value="<?= $producto['unidades'] ?>" step="1" min="0" max="<?= $producto['existencia'] ?>" disabled="disabled" /></li>
        <li>Importe: <?= $producto['precio'] * $producto['unidades'] ?> </li>
        <li><input type="button" class="bt-compra" value="comprar" data-reg="<?= $producto['codigo'] ?>" /></li>
        <li><input type="button" class="bt-borra" value="borrar" data-reg="<?= $producto['codigo'] ?>" /></li>

      </ul>

    </div>

    <?php
  }
} else {
  echo "El carrito est&aacute; vacio.<br>";
}
?>

<input type="button" name="irCatalogo" id="irCatalogo" value="Volver al catalogo de productos" />
<br>
<input type="button" name="salirCat" id="salirCat" value="salir de la aplicacion" />
<br><br>
<output id="msg" ></output>
