<?php
class Conexion
{
   protected $conexion;
   protected $dsn, $username, $password;

   public function __construct(  )
   {
     $a = func_get_args();
//   $i = func_num_args();
       $this->dsn = $a[0] ; //  $dsn;
       $this->username = $a[1] ; // $username;
       $this->password = $a[2] ; // $password;
       $this->connect();
   }

   private function connect()
   {

//     echo "<br>" . $this->dsn . " salidas <br>";
       $this->conexion = new PDO($this->dsn, $this->username, $this->password);
   }

   public function __sleep()
   {
       return array('dsn', 'username', 'password');
   }

   public function __wakeup()
   {
       $this->connect();
   }
}
?>
