<?php
/**
 *
 */
interface DAO
{
  public function listarArticulos();
  public function validaUsuario($usuario , $clave);
  public function obtenCarrito($usuario);
  public function anadirCarrito($usuario , $articulo , $unidades);
  public function comprobarEnCarrito($usuario , $articulo );
  public function cambiaUnidadesCarrito($usuario , $articulo , $unidades);
  public function comprobarStock( $articulo );
  public function borrarDelCarrito($usuario , $articulo );
  public function comprar($usuario , $articulo , $unidades);

}
