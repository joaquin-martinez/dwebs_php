<?php
class AccionComprar implements EjecutaAccion {

  function ejecutar(){
    session_start();
    $dao = unserialize($_SESSION['dao']);
    $listaProd = $dao->comprobarStock( $_REQUEST['articulo'] );
    $remanente = $listaProd[0] - $_REQUEST['unidades'];
    if($dao->comprar($_SESSION['usuario'] , $_REQUEST['articulo'] , $remanente)){
      echo "Compra realizada correctamente.";
    } else {
      echo "Compra anulada.<br> Hemos tenido problemas.<br> Disculpe las molestias.";
    }

  }

}
