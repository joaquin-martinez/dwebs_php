<?php
require_once('../modelo/DAO.php');
require_once('../modelo/Conexion.php');

class DAOImpl extends Conexion implements DAO{

  public function __construct($datosbase, $usuario, $contrasena){
    parent::__construct($datosbase, $usuario, $contrasena);
  }

  public function listarArticulos(){

    $resul = $this->conexion->query("select * from articulos");

    $reserv = $this->conexion->query("select codigo , sum(unidades) as unidades from carrito where pagado = false group by codigo");

    while($filaArticulos = $resul->fetch()){
      $listaArticulos[] = $filaArticulos;
    }

    while($filaReservas = $reserv->fetch()){
      $listaReservas[] = $filaReservas;
    }

    foreach( $listaReservas as $lineaRes ){

      foreach( $listaArticulos as $key => $lineaArt ){
        if($lineaArt["codigo"] == $lineaRes["codigo"]){
          $listaArticulos[$key]["existencia"] -= $lineaRes["unidades"];
        }
      }
    }
    return $listaArticulos;

  }
  public function validaUsuario($usuario , $clave){
    $validado;

    $usr = $this->conexion->query("select * from usuarios where usuario='$usuario' and password = '$clave'");
    if($usr->rowCount() > 0){
      $this->validado= true;
      while($linea = $usr->fetch()){
      }
    }else{
      $this->validado= false;
    }
    return $this->validado;

  }
  public function obtenCarrito($usuario){

    $resul = $this->conexion->query("select * from carrito as c , articulos as a where c.codigo=a.codigo and usuario='$usuario' and pagado = false");
    if($resul->rowCount() > 0){
      while($filaCarrito = $resul->fetch()){
        $listaCarrito[] = $filaCarrito;
      }
    } else {
      throw new \Exception("Carrito vacio. ", 1);

    }

    return $listaCarrito;

  }
  public function anadirCarrito($usuario , $articulo , $unidades ){
    $query = "insert into carrito ( usuario , codigo , unidades , pagado) values ('$usuario' , '$articulo' , '$unidades' , false )";
    $this->conexion->exec($query);
  }

  public function comprobarEnCarrito($usuario , $articulo ){


    $resul = $this->conexion->query("select unidades from carrito where codigo='$articulo' and usuario='$usuario' and pagado = false");
    if($resul->rowCount() > 0){
      $unidadesReser = $resul->fetch();
      var_dump($unidadesReser);
    } else {
      $unidadesReser[0] = 0;
    }
    echo "<br>ya hay estas unidades = " . $unidadesReser[0];
    return $unidadesReser;

  }
  public function cambiaUnidadesCarrito($usuario , $articulo , $unidades){

    $actualizaUnidades = "update carrito set unidades = $unidades where usuario like '$usuario' and codigo like '$articulo' and pagado = false";
    $nreg = $this->conexion->exec($actualizaUnidades);
    return $nreg;

  }

  public function comprobarStock( $articulo ){

    $resStok = $this->conexion->query("select existencia from articulos where codigo ='$articulo'");
    $stock = $resStok->fetch();
    return $stock;
  }

  public function borrarDelCarrito($usuario , $articulo){

    $queryBorrar = "delete from carrito where usuario='$usuario' and codigo = '$articulo' and pagado = false";
    $nreg = $this->conexion->exec($queryBorrar);
    return $nreg;

  }

  public function comprar($usuario , $articulo , $unidades){

    $correcto = TRUE;
    $qryArticulos = "update articulos set existencia ='$unidades' where codigo = '$articulo'";
    $qryCarrito = "update carrito set pagado = true where usuario ='$usuario' and codigo = '$articulo'";

    $this->conexion->beginTransaction();
    if($this->conexion->exec($qryArticulos) == 0){
      $correcto = FALSE;
    }
    if($this->conexion->exec($qryCarrito) == 0){
      $correcto = FALSE;
    }


    if($correcto){
      $this->conexion->commit();
    }else {
      $this->conexion->rollback();
    }
    return $correcto;
  }

}
