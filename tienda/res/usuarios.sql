DROP DATABASE IF EXISTS tienda;
CREATE DATABASE tienda CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE tienda;

-- TABLAS

DROP TABLE IF EXISTS articulos;
CREATE TABLE articulos (

	codigo varchar(30) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(50) NOT NULL,
	existencia double NOT NULL,
	precio double NOT NULL,
	PRIMARY KEY (codigo)
);

DROP TABLE IF EXISTS usuarios;
create table usuarios(

  usuario varchar(20) not null primary key,
  password varchar(20) not null
);

DROP TABLE IF EXISTS carrito;
CREATE TABLE carrito (
  id int NOT NULL auto_increment ,
  codigo varchar(30) NOT NULL,
  usuario varchar(20) not null,
  unidades int(5) NOT NULL,
  pagado boolean NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fkusuarios FOREIGN KEY (usuario) REFERENCES usuarios(usuario),
	CONSTRAINT fkarticulos FOREIGN KEY (codigo) REFERENCES articulos(codigo)

);

-- DATOS

INSERT INTO articulos ( codigo, nombre, descripcion, existencia, precio) VALUES
( 'EC003', 'CAJA LAZOO', 'CAJA 100X200', 10, 1.25),
( 'EC001', 'ESFERO ROJO', 'ESFERO BORRABLE', 40, 0.65 ),
( 'EC002', 'ESFERO NEGRO', 'ESFERO BORRABLE', 30, 0.65 ),
( 'FA001', 'FOLDER ARCHIVADOR', 'FOLDER CARTON', 10, 2.79 ),
( 'FA002', 'FOLDER ANILLAS', 'SEPARADORA', 20, 3.79 ),
( 'SM001', 'SOBRE MANILA ', 'SOBRE MANILA OFICIO', 15, 0.1 );



insert into usuarios (  usuario , password )
  values  ( "joa" , "joaquin"),
          ( "joaquin" , "joaquin"),
					( "usuario" , "clave"),
          ( "jmp" , "joaquin");

insert into carrito ( id , codigo , usuario , unidades , pagado )
  values  (1 , 'EC002', "joa" , 3 , false ),
          (2 ,'FA001', "joaquin" , 2 , false),
					(3 , 'EC001', "joa" , 3 , false ),
          (4 ,'SM001', "jmp" , 1 , false);
