<?php
class Carrito {
    private $items;
    
    public function __construct() {
        $this->items = array();        
    }
    
    public function agregar_item($tipo , $num){
        if(isset($this->items[$tipo]))
            $this->items[$tipo] += $num;
        else 
            $this->items[$tipo] = $num;
        
    }
    
    public function retirar_item($tipo , $num){
        $value = true;
        
        if(isset($this->items[$tipo])){
            if ($num<$this->items[$tipo]) {
                $this->items[$tipo] -= $num;
            }elseif ($this->items[$tipo] == $num)
            unset($this->items[$tipo]);
            else
                $value=false;
        }
        else
            $value = false;
        
        return $value;
        
    }
    
    public function ver_carrito_json(){
        header("Content-Type: application/json");
        echo json_encode($this->items);
        
        
    }
}