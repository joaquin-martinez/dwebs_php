var datos;
var BASE_URL = 'http://localhost/carrito';

var app = {

	init: function() {
		app.getItems();
	},

	getItems: function() {
		$.ajax({
			type: 'GET',
			url: BASE_URL + '/principal.php',
			dataType: 'json',
			success: function(data){	
				datos = data;
				app.listarArticulos(data);
			},
			error: function(error){
				console.log(error);
			}
		});
	}, 
	
	insertar: function() {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/principal.php',
			data: { 
				action     : "insertar",
				articulo   : $("#articulo").val(),
				cantidad     : $("#cantidad").val()
			},
			dataType: 'json',
			success: function(data){
				app.listarArticulos(data);
			},
			error: function(error){
				console.log(error);
			}
		});
	
		$("#articulo").val("");
		$("#cantidad").val("");
		 
	},
	
	modificar: function() {

		$.ajax({
			type: 'POST',
			url: BASE_URL + '/principal.php',
			data: {
				action     : "modificar",
				articulo        : $("#articulo").val(),
				cantidad     : $("#cantidad").val(),
				cantidadIni	: datos[$("#articulo").val()]
			},
			dataType: 'json',
			success: function(data){
				app.listarArticulos(data);
			},
			error: function(error){
				console.log(error);
			} 
		});
		
		
		$("#articulo").val("");
		$("#cantidad").val("");
		 

		
	},
	
	eliminar: function(articulo , cantidad) {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/principal.php',
			data: {
				action     : "eliminar",
				articulo   : articulo,
				cantidad	: cantidad
			},
			dataType: 'json',
			success: function(data){
				app.listarArticulos(data);
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	
	listarArticulos: function(articulos) {
		// Vaciado de subelementos del listview y categorias
		$("#tblListar").empty();
		
		// Representación: generación de cabecera
		$("#tblListar").html(
		        "<thead>"+
		        "   <tr>"+
		        "   <th>articulo</th>"+
		        "   <th>cantidad</th>"+
		        "   <th></th>"+
		        "   </tr>"+
		        "</thead>"+
		        "<tbody>"+
		        "</tbody>"
		        );
		
		// Por cada elemento de articulos se representa en el listview
		$.each(articulos, function(index, value) {
			// Elemento del listview
			$("#tblListar tbody").append("<tr>");
	        $("#tblListar tbody").append("<td>" + index + "</td>");
	        $("#tblListar tbody").append("<td>" + value + "</td>");
        
	        $("#tblListar tbody").append("<td><img src='img/edit.png' alt='"+index+"' data-valor='" + value +  "' class='btnEditar' title='editar'/><img src='img/delete.png' alt='"+index+"' title='eliminar' class='btnEliminar'/></td>");
	        $("#tblListar tbody").append("</tr>");
	    });
	}
}

$(document).ready(function() {
    var operacion;
    
	$("#Guardar").on("click", function(){
		if (operacion == "MODIFICAR")
			app.modificar();
		else
			app.insertar();
	});

	$("#tblListar").on("click", ".btnEditar", function(){
		
		indice_seleccionado = $(this).attr("alt"); 
		var value = $(this).attr("data-valor");
		$("#articulo").val(indice_seleccionado);
		$("#cantidad").val(value);
		operacion = "MODIFICAR";
	});
	
	$("#tblListar").on("click", ".btnEliminar",function(){
		indice_seleccionado = $(this).attr("alt"); 
		var value = datos[indice_seleccionado];
		app.eliminar(indice_seleccionado , value);
	});
});