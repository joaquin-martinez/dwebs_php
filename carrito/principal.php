<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
    
});

$c= new Carrito();
$c->agregar_item("gafas", 3);
$c->agregar_item("peluche", 4);
$c->agregar_item("revista", 2);
$c->agregar_item("gafas", 1);
$c->agregar_item("gafas", 2);
// $c->agregar_item("gafas", 3);


if(isset($_REQUEST["action"]) && !empty($_REQUEST["action"])){

$accion = $_REQUEST["action"];
switch ($accion){
    case "insertar":
        insertarElemento($c);
        break;
    case "eliminar":
        eliminarElemento($c);
        break;
    case "modificar":
        modificarElemento($c);
        break;
}

}



$c->ver_carrito_json();


function insertarElemento($c){
    $tipo = $_REQUEST["articulo"];
    $num = $_REQUEST["cantidad"];
    $c->agregar_item($tipo , $num);
}

function eliminarElemento($c){
    $tipo = $_REQUEST["articulo"];
    $num = $_REQUEST["cantidad"];
    $c->retirar_item($tipo , $num);
}

function modificarElemento($c){
    $numIni = $_REQUEST["cantidadIni"];
    $tipo = $_REQUEST["articulo"];
    $num = $_REQUEST["cantidad"];
    $c->retirar_item($tipo , $numIni);
    $c->agregar_item($tipo , $num);
}



