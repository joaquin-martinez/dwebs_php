<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Mini diccionario</title>
</head>
<body>
<?php
if(isset($_REQUEST["palabra"]) && !empty($_REQUEST["palabra"])){
  $vocablo = $_REQUEST["palabra"] ;
  $vocablo = strtolower($vocablo);
$primeravez = false;
$diccionario = array("mesa"=>"table" , "coche"=>"car" , "lapiz"=>"pencil"
, "mano"=>"hand" , "limon"=>"lemon" , "vaso"=>"glass" , "libro"=>"book"
, "naranja"=>"orange" , "ventana" =>"window" , "cocina"=>"kitchen");
}else{
  $vocablo = "";
  $primeravez = true;

}



 ?>


  <p>Esta aplicacion le traduce al ingles las siguientes palabras:</p>
  <ul>
    <li>mesa</li>
    <li>coche</li>
    <li>limon</li>
    <li>mano</li>
    <li>vaso</li>
    <li>naranja</li>
    <li>ventana</li>
    <li>lapiz</li>
    <li>libro</li>
    <li>cocina</li>
  </ul>

  <form>
    <label for="palabra" >Introduzca una de las palabras.</label>
    <input type="text" name="palabra" id="palabra" value= "<?=$vocablo?>"/>
    <input type="submit" name="enviar" value="enviar" id="enviar" />
  </form>

<?php
  if(!$primeravez){
//    $vocablo = $_REQUEST["palabra"] ;
    if(isset($diccionario[$vocablo])){
      $solucion = $diccionario[$vocablo];
    ?>
    <br><br>
    el resultado es: <?= $solucion ?>
    <br><br>
    <?php
    echo "Ha introducido $vocablo y su traduccion es $diccionario[$vocablo] " . "<br><br>" ;
  }  else{
      echo "debe introducir una palabra valida";
}
  }
/*
  else{
    echo "debe introducir una palabra valida";
  }
  */
?>

</body>
</html>


<?php
/*
Desarrollar una aplicación web mini-diccionario que a partir de una palabra
 en español introducida por el usuario resolverá su correspondiente traducción
  en inglés (implementar el ejercicio usando una única estructura de array
   asociativo, con al menos, 10 palabras).
*/
