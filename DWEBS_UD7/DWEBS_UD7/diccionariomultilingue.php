<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Mini diccionario</title>
</head>
<body>
<?php
if(isset($_REQUEST["palabra"]) && !empty($_REQUEST["palabra"])){
  $vocablo = $_REQUEST["palabra"] ;
  $vocablo = strtolower($vocablo);
  $idioma = $_REQUEST["idioma"] ;
$primeravez = false;
$diccionario = array("mesa"=>array("ingles"=>"table" , "frances"=>"table") ,
  "coche"=>array("ingles"=>"car" , "frances"=>"voiture"),
  "lapiz"=>array("ingles"=>"pencil" , "frances"=>"crayon"),
  "mano"=>array("ingles"=>"hand" , "frances"=>"main"),
  "limon"=>array("ingles"=>"lemon" , "frances"=>"citron"),
  "vaso"=>array("ingles"=>"glass" , "frances"=>"verre"),
  "libro"=>array("ingles"=>"book" , "frances"=>"livre"),
  "naranja"=>array("ingles"=>"orange" , "frances"=>"orange"),
  "ventana" =>array("ingles"=>"window" , "frances"=>"fenêtre"),
  "cocina"=>array("ingles"=>"kitchen" , "frances"=>"cuisine"));
}else{
  $vocablo = "";
  $primeravez = true;
}
 ?>
  <p>Esta aplicacion le traduce las siguientes palabras:</p>
  <ul>
    <li>mesa</li>
    <li>coche</li>
    <li>limon</li>
    <li>mano</li>
    <li>vaso</li>
    <li>naranja</li>
    <li>ventana</li>
    <li>lapiz</li>
    <li>libro</li>
    <li>cocina</li>
  </ul>

  <form>
    <label for="idioma" >Seleccione un idioma.</label>
    <select name="idioma" id="idioma" >
      <option value="ingles" >Ingles</option>
      <option value="frances" >Frances</option>
    </select>
    <br/>  <br/>
    <label for="palabra" >Introduzca una de las palabras.</label>
    <input type="text" name="palabra" id="palabra" value= "<?=$vocablo?>"/>
    <input type="submit" name="enviar" value="enviar" id="enviar" />
  </form>

<?php
  if(!$primeravez){
    if(isset($diccionario[$vocablo])){
      $solucion = $diccionario[$vocablo][$idioma];
    ?>
    <br><br>
    el resultado es: <?= $solucion ?>
    <br><br>
    <?php
    echo "Ha introducido $vocablo y su traduccion al $idioma es $solucion " . "<br><br>" ;
  }  else{
      echo "debe introducir una palabra valida";
}
  }
?>
</body>
</html>

<?php
/*
Desarrollar una aplicación web mini-diccionario que a partir de una palabra
 en español introducida por el usuario resolverá su correspondiente traducción
  en inglés (implementar el ejercicio usando una única estructura de array
   asociativo, con al menos, 10 palabras).
*/
