<!doctype html>
<html>
   <head>
      <meta charset="utf-8"/>
   </head>
   <body>
      <?php
         define("N", rand(1, 10));

         $a = array(); // $a[];

         for ($i = 0; $i < N; $i++)
          $a[$i] = rand(0, N - 1) + 1;

         if (defined("N") && is_array($a)) {
          echo "<span>ARRAY CON " . N . " VALORES:</span>";
          echo "<table>";

          foreach ($a as $elem) {
              echo "<tr><td>" . $elem . "</td></tr>";
          }

          echo "</table>";

          var_dump($a);
         }

         ?>
   </body>
</html>
