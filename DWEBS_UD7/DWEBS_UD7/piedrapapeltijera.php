<?php
header("content-type:text/html;charset=utf-8");
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Piedra, papel o tijera.</title>
</head>
<?php
$emogis = array("piedra1" => "&#x1F91C" , "piedra2" => "&#x1F91B" ,
"papel" => "&#x1F91A" , "tijera" => "&#x1F596");

$tirada1 = rand(1,3);
$tirada2 = rand(2,4);

$valores = array( 1 => "piedra1" , 2 => "papel" , 3 => "tijera" , 4 => "piedra2" );

if(($tirada1 == $tirada2) || ($tirada1 == 1 && $tirada2 == 4 )){
  $gana = "Han empatado";
}elseif (( $tirada1 == 1 && $tirada2 == 3) || ($tirada1 == 2 && $tirada2 == 4) ||
($tirada1 == 3 && $tirada2 == 2 )){
  $gana ="Ha ganado el jugador 1.";
}else{
  $gana ="Ha ganado el jugador 2.";
}

$emo1 = $emogis[$valores[$tirada1]];
$emo2 = $emogis[$valores[$tirada2]];

?>
<body>

  <p> Actualice la p&aacute;gina para mostrar otra partida. </p>

  <table>
    <tr>
      <th >Jugador 1</th>
      <th >Jugador 2</th>
    </tr>
    <tr>
      <td><span style="font-size: 7em" ><?=$emo1?></span></td>
      <td><span style="font-size: 7em" ><?=$emo2?></span></td>
    </tr>
    <tr>
      <th colspan="2" > <?=$gana?> </th>
    </tr>
  </table>

</body>
</html>
